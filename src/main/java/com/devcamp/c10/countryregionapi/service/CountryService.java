package com.devcamp.c10.countryregionapi.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import com.devcamp.c10.countryregionapi.models.Country;

@Service
public class CountryService {
    @Autowired
    private RegionService regionservice;

   

    public ArrayList<Country> getAllCountries() {
        ArrayList<Country> allCountry = new ArrayList<Country>();

        Country vietNam = new Country("VN", "Viet Nam");
        Country us = new Country("US", "My");
        Country japan = new Country("JP", "Nhat Ban");
    
        Country china = new Country("CN", "Trung Quoc");
        Country thailad = new Country("TL", "Thai Lan");
        Country korea = new Country("KRA", "Han Quoc");
        
        vietNam.setRegions(regionservice.getRegionVietNam());
        us.setRegions(regionservice.getRegionUS());
        japan.setRegions(regionservice.getRegionJapan());

        china.setRegions(regionservice.getRegionChina());
        thailad.setRegions(regionservice.getRegionThaiLan());
        korea.setRegions(regionservice.getRegionKorea());

        allCountry.add(vietNam);
        allCountry.add(us);
        allCountry.add(japan);
        allCountry.add(china);
        allCountry.add(thailad);
        allCountry.add(korea);

        return allCountry;
    }
    public Country getCountryInfo(@RequestParam(name="code", required = true) String countryCode){
        ArrayList<Country> allCountry = getAllCountries();

        Country findCountry = new Country();

        for (Country countryElement : allCountry) {
            if(countryElement.getCountryCode().equals(countryCode)){
                findCountry = countryElement;
            }
        }
        return findCountry;
    }

    public Country getCountryByIndex(int index){
        ArrayList<Country> allCountry = getAllCountries();

        if(index >= 0 && index <= allCountry.size()) {
            return allCountry.get(index);
        }else{
            return null;
        }
    }
}
