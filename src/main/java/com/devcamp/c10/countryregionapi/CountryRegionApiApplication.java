package com.devcamp.c10.countryregionapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CountryRegionApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CountryRegionApiApplication.class, args);
	}

}
