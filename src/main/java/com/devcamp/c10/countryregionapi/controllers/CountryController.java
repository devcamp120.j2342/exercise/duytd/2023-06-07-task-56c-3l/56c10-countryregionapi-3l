package com.devcamp.c10.countryregionapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.c10.countryregionapi.models.Country;
import com.devcamp.c10.countryregionapi.service.CountryService;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class CountryController {
    @Autowired
    private CountryService countryservice;

    @GetMapping("/countries")
    public ArrayList<Country> getAllCountries() {
        ArrayList<Country> allCountry = countryservice.getAllCountries();

        return allCountry;
    }

    @GetMapping("/country-info")
    public Country getCountryInfo(@RequestParam(name = "code", required = true) String countryCode) {

        Country findCountry = countryservice.getCountryInfo(countryCode);
        return findCountry;
    }

    @GetMapping("/countries/{index}")
    public Country getCountryByIndex(@PathVariable() int index){
        return countryservice.getCountryByIndex(index);

    }
}
