package com.devcamp.c10.countryregionapi.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.c10.countryregionapi.models.Region;
import com.devcamp.c10.countryregionapi.service.RegionService;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class RegionColtroller {
    @Autowired
    private RegionService regionService;

    @GetMapping("/region-info")
    public Region getRegionInfo(@RequestParam(name="code", required = true) String regionCode){
        Region findRegion = regionService.filterRegion(regionCode);

        return findRegion;
    }
}
